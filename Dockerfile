FROM openjdk:13-alpine
VOLUME /tmp
ADD /target/*.war test.war
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-war","/test.war"]

