package com.gitlad;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller                                           
public class SimpleController {                        
                                                      
	@GetMapping("/api")                                  
	@ResponseBody                                     
	public String index() {                           
		return "Hello World - Developer ";  
	}                                                 
                                                      
}
